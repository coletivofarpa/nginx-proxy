# NGINX como um proxy reverso para HTTP e outros protocolos de rede

### Cria uma camada de rede de proteção das aplicações por detrás da rede proxy reverso

O proxy é normalmente usado para distribuir a carga entre vários servidores, mostrar perfeitamente o conteúdo de diferentes sites ou
passar solicitações de processamento para servidores de aplicativos por meio de protocolos diferentes do HTTP.

Um proxy reverso (_reverse proxy_, in english) é um servidor que fica na frente dos servidores web e encaminha as solicitações do cliente (por exemplo, navegador web) para esses servidores web.

Os proxy reversos normalmente são implementados para ajudar a aumentar a segurança, o desempenho e a confiabilidade.

## Documentação oficial

- Nginx proxy https://github.com/nginx-proxy/nginx-proxy
- Site doc https://docs.nginx.com/nginx/admin-guide/web-server/reverse-proxy/
- Nginx proxy companion https://github.com/nginx-proxy/docker-letsencrypt-nginx-proxy-companion/
